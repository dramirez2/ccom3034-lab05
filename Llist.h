/*
	Authors: Daniel Ramirez, Ramon Luis
	emails: bojangles7856@gmail.com, rlcmartis@gmail.com
*/
#ifndef LLIST_H
#define LLIST_H

#include <iostream>
#include <QPoint>
#include<QPainter>
#include <QMouseEvent>
#include <cmath>
using namespace std;

typedef QPoint ElementType ;


class Node {
public:
    ElementType data;
    Node* next;
public:

    // Constructor, can call with 0, 1, or 2 params.
    Node() {};

    // display with address for class illustration purposeds
    friend QDebug operator<< (QDebug d, const Node &n);

// Make Llist a friend so that we can access all Node objects info.
friend class LList;
};

class LList {
private:
    Node* first;
    int   mySize;
public:
    // Constructor.
    LList(): first(NULL), mySize(0) {}
    
    void insert(ElementType val, int pos);
    
    // erase the node at position pos.
    void erase(int pos); 
    
    void display(ostream &out) const;

    void push(ElementType e);

    bool operator==(const LList& L) const;

    friend QDebug operator<< (QDebug d, const LList &n);

    void paint(QPainter& p);

    ElementType euclidianDist(QMouseEvent *event);

    int coordPos(ElementType coords);
};

ostream & operator<< (ostream &, const LList &);

#endif
