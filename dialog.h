#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtDebug>
#include <QtGui>
#include <QtCore>
#include "Llist.h"

class Dialog : public QDialog
{
    Q_OBJECT

private:
    LList Points;
public:
    Dialog(QWidget *parent = 0);
    ~Dialog();

protected:
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
};



#endif // DIALOG_H
