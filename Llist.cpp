#include "Llist.h"
#include <QDebug>
#include<QPainter>
#include <cmath>

void LList::insert(ElementType val, int pos) {
    
    // Validate position
    if ( (pos < 0) || (pos > mySize) ) { 
        cerr << "Attempting insert at illegal position " << pos << endl;
        exit(1);
    }
    
    // create the new node to insert
    Node *n = new Node();
    n->data.setX(val.x());
    n->data.setY(val.y());
    if (pos == 0) { 
        // inserting at the first position
        n->next = first;
        first = n;
    } else {
        Node *curr = first;
        // move to the position
        for (int i=1; i<pos; i++) curr = curr->next;  
        n->next = curr->next;
        curr->next = n;
    }
    mySize++;    
}
    
void LList::erase(int pos) {

    // Validate position
    if ( (pos < 0) || (pos > mySize - 1) ) { 
        cerr << "Attempting delete at illegal position " << pos << endl;
        exit(1);
    }
    
    Node* tmp;
    if (pos == 0) { 
        // erasing the node at the first position
        tmp = first;
        first = first->next;
    } else {
        Node *curr = first;
        // move to the position before the node to be erased
        for (int i=1; i<pos; i++)   curr = curr->next;  
        tmp = curr->next;
        curr->next = curr->next->next;
    }
    delete tmp;

    mySize--; 
}



bool LList::operator==(const LList& L) const {
    if (mySize != L.mySize) return false; 
    Node *p = first;
    Node *q = L.first;
    while (p != NULL) {
        if (p->data != q->data) return false;
        p = p->next;
        q = q->next;
    }
    return true;
}


// Operator << overload for the nodes.
// Allows the use of qDebug() << n;  where n is a Node.

QDebug operator<< (QDebug d, const Node &n) {
    d << n.data; //<< "," << n.data.y();
    return d;
}


// Operator << overload for the List.
// Allows the use of qDebug() << L;  where L is a LList.

QDebug operator<< (QDebug d, const LList &n) {
    Node *p = n.first;
    while (p!=NULL) {
        d << *p;
        p = p->next;
    }
    return d;
}

// The classic push: inserts at the end of the list.

void LList::push(ElementType e) {
    qDebug() << "inserting " << e << " at pos: " << mySize ;
    insert(e,mySize);
}

void LList::paint(QPainter& p){
    Node *a=first;

    while(a != NULL){

        p.drawEllipse(a->data.x(),a->data.y(),20,20);
        a=a->next;
    }
}

//Recieves point.  goes tru all nodes, calculates the closest distance
//and returns the position of the closest coordinate
int LList::euclidianDist(QMouseEvent * event){
     //Assumes that the first position is the
     int minpos=0;

     //Holds the min and temporary distances.
     double mindist, temp;

     //Will go thru each node on the list to compare.
     Node *p=first;

     //If the size is 0, it will return it by default so the program doesn't
     //delete an illegal position.
     if (mySize == 0) return minpos;

     //Asumes the first node has the closest coordinates.
     mindist= sqrt(pow((double)(event->x() - p->data.x()),2) + pow((double)(event->y() - p->data.y()),2));

        for(int i=0; i < mySize; i++,p=p->next){

            //Temp holds each node's dist relative to Coords
            temp=  sqrt(pow((double)(event->x() - p->data.x()),2) + pow((double)(event->y() - p->data.y()),2));

            //If temp is smaller than mindist, then mindist is now temp's mindist
            if(temp < mindist){
                mindist = temp;
                minpos=i;
            }
        }
return minpos;
}
