#include "dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
}

Dialog::~Dialog()
{

}


void Dialog::mousePressEvent ( QMouseEvent * event ) {

    // output the x,y coordinates
    qDebug() << "clicked at: " << event->x() << "," << event->y();
    if (event->button() == Qt::LeftButton){
        //Pushes the x and y coordinates on the last position
        Points.push(QPoint(event->x(),event->y()));

        //Shows the coordinates on the debug screen
        qDebug() << Points;
        repaint();

    }
    if (event->button() == Qt::RightButton){
       //Holds the position of the found coordinates
       int newpos;

       //Calls function that returns the coordinates
       newpos = Points.euclidianDist(event);

       //Erases the position to be erased
       Points.erase(newpos);

       //Paints the list without the removed points
       repaint();

       //Displays message
       qDebug() << Points << endl;


    }
}

void Dialog::paintEvent(QPaintEvent *event) {
    // the QPainter is the 'canvas' to which we will draw
    // the QPen is the pen that will be used to draw to the 'canvas'
    QPainter p(this);
    QPen myPen;
    myPen.setWidth(10);
    myPen.setColor(QColor(0xff0000));
    p.setPen(myPen);
    Points.paint(p);
    //p.drawEllipse(width()/2,height()/2,20,20);
}
